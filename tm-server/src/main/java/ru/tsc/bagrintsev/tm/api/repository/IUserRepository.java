package ru.tsc.bagrintsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.Collection;
import java.util.List;

@Mapper
public interface IUserRepository extends IAbstractRepository<User> {

    @Insert("INSERT INTO m_user " +
            "(id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked) " +
            "VALUES " +
            "(#{id}, #{login}, #{passwordHash}, #{passwordSalt}, #{email}, #{firstName}, #{middleName}, #{lastName}, #{role}, #{locked});")
    void add(@NotNull final User model);

    @Insert({
            "<script>",
            "INSERT INTO m_user",
            "(id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked) ",
            "VALUES" +
                    "<foreach item='item' collection='records' open='' separator=',' close=''>" +
                    "(" +
                    "#{item.id},",
            "#{item.login},",
            "#{item.passwordHash},",
            "#{item.passwordSalt},",
            "#{item.email},",
            "#{item.firstName},",
            "#{item.middleName},",
            "#{item.lastName},",
            "#{item.role},",
            "#{item.locked}" +
                    ")" +
                    "</foreach>",
            "</script>"})
    void addAll(@Param("records") @NotNull final Collection<User> records);

    @Override
    @Delete("DELETE " +
            "FROM m_user;")
    void clearAll();

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked " +
            "FROM m_user;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "passwordSalt", column = "password_salt"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "is_locked")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked " +
            "FROM m_user " +
            "WHERE email = #{email};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "passwordSalt", column = "password_salt"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "is_locked")
    })
    User findByEmail(@NotNull final String email);

    @Nullable
    @Select("SELECT id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked " +
            "FROM m_user " +
            "WHERE login = #{login};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "passwordSalt", column = "password_salt"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "is_locked")
    })
    User findByLogin(@Param("login") @NotNull final String login);

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, password_salt, email, first_name, middle_name, last_name, role, is_locked " +
            "FROM m_user " +
            "WHERE id = #{id};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "passwordSalt", column = "password_salt"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "is_locked")
    })
    User findOneById(@NotNull final String id);

    @Delete("DELETE " +
            "FROM m_user " +
            "WHERE login = #{login};")
    void removeByLogin(@NotNull final String login);

    @Update("UPDATE m_user " +
            "SET login = #{login}, " +
            "password_hash = #{passwordHash}, " +
            "password_salt = #{passwordSalt}, " +
            "email = #{email}, " +
            "first_name = #{firstName}, " +
            "middle_name = #{middleName}, " +
            "last_name = #{lastName}, " +
            "role = #{role}, " +
            "is_locked = #{locked} " +
            "WHERE id = #{id};")
    void setParameter(@NotNull final User user);

    @Update("UPDATE m_user " +
            "SET role = #{role} " +
            "WHERE login = #{login};")
    void setRole(
            @Param("login") @NotNull final String login,
            @Param("role") @NotNull final Role role
    );

    @Update("UPDATE m_user " +
            "SET password_hash = #{password}," +
            "password_salt = #{salt} " +
            "WHERE login = #{login};")
    void setUserPassword(
            @Param("login") @NotNull final String login,
            @Param("password") @NotNull final String password,
            @Param("salt") final byte @NotNull [] salt
    );

    @Override
    @Select("SELECT count(*)" +
            "FROM m_user;")
    long totalCount();

}
