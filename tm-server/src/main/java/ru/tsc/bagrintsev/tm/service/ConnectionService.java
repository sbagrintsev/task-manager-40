package ru.tsc.bagrintsev.tm.service;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String userName = propertyService.getDatabaseUserName();
        @NotNull final String userPassword = propertyService.getDatabaseUserPassword();
        @NotNull final String url = propertyService.getDatabaseUrl();
        @NotNull final String driver = propertyService.getDatabaseDriver();
        @NotNull final DataSource dataSource = new UnpooledDataSource(driver, url, userName, userPassword);
        @NotNull final TransactionFactory factory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", factory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
