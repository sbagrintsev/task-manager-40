package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

}
